<?php

namespace Drupal\ctek_search\Solr;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ctek_common\Model\ModelPluginManager;
use Drupal\ctek_search\Constants;
use Drupal\ctek_search\SolrModelInterface;
use Drupal\ctek_search\SolrModelPluginManager;
use Solarium\QueryType\Update\Query\Query;

class Indexer {
  use StringTranslationTrait;

  public static function getIndexCacheId($definition) {
    return "ctek_search:index:{$definition['entityType']}:{$definition['bundle']}";
  }

  protected $client;

  protected $solrModelPluginManager;

  protected $entityTypeManager;

  protected $logger;

  protected $cache;

  protected $enabled = TRUE;

  public function __construct(
    Client $client,
    SolrModelPluginManager $solrModelPluginManager,
    EntityTypeManagerInterface $entityTypeManager,
    ModelPluginManager $modelPluginManager,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->client = $client;
    $this->solrModelPluginManager = $solrModelPluginManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get(Constants::LOGGER_CHANNEL);
  }

  public function getEnabled() : bool {
    return $this->enabled;
  }

  public function setEnabled(bool $status) : void {
    $this->enabled = $status;
  }

  protected function ensureEnabled() : void {
    if (!$this->enabled) {
      throw new \LogicException('Indexer is disabled.');
    }
  }

  /**
   * @param $definition
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDbQueryForIndex($definition) : QueryInterface {
    $storageDefinition = $this->entityTypeManager->getDefinition($definition['entityType']);
    $query = $this->entityTypeManager->getStorage($definition['entityType'])->getQuery();
    $query->condition($storageDefinition->getKey('bundle'), $definition['bundle']);
    $query->sort($storageDefinition->getKey('id'));
    $alterMethod = $definition['class'] . '::alterIndexQuery';
    if (is_callable($alterMethod)) {
      call_user_func_array($alterMethod, [$query]);
    }
    return $query;
  }

  public function getDbCountForIndex($definition) : int {
    return $this->getDbQueryForIndex($definition)->count()->execute();
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $throwOnNonModel
   *
   * @return \Drupal\ctek_common\Model\ModelInterface|null
   */
  protected function getIndexableModelFromEntity(ContentEntityInterface $entity, bool $throwOnNonModel) {
    $model = $this->solrModelPluginManager->wrap($entity);
    if (!$model instanceof SolrModelInterface) {
      if ($throwOnNonModel) {
        throw new \InvalidArgumentException('$entity either has no model wrapper, or the model does not implement ' . SolrModelInterface::class);
      }
      return NULL;
    }
    return $model;
  }

  /**
   * @param \Drupal\ctek_search\SolrModelInterface $model
   *
   * @return SolrModelInterface[]
   */
  protected function collectRelatedModels(SolrModelInterface $model) : array {
    $relatedModels = [];
    foreach ($model->getRelated() as $relatedModel) {
      $relatedModels[$relatedModel->idForIndexing()] = $relatedModel;
      $relatedModels += $this->collectRelatedModels($relatedModel);
    }
    return $relatedModels;
  }

  /**
   * @param array $definition
   * @param bool $commit
   */
  public function clearIndex(array $definition, bool $commit = TRUE) : void {
    $this->ensureEnabled();
    $update = $this->client->createUpdate();
    $conditions = [
      'site_s' => $_ENV['CTEK_SITE_NAME'],
      'environment_s' => $_ENV['CTEK_ENV'],
      'entity_type_s' => $definition['entityType'],
      'bundle_s' => $definition['bundle'],
    ];
    $i = 1;
    $query = array_reduce(array_keys($conditions), function($carry, $key) use ($conditions, &$i) : string {
      $condition = "$key: %P$i%";
      $i++;
      if (!$carry) {
        return $condition;
      }
      return "$carry AND $condition";
    });
    $update->addDeleteQuery($query, array_values($conditions));
    if ($commit) {
      $update->addCommit();
    }
    $this->client->update($update);
  }

  /**
   * @param array $entities
   * @param bool $commit
   * @param bool $indexRelated
   * @param bool $invalidateCache
   */
  public function indexEntities(array $entities, bool $commit = TRUE, bool $indexRelated = TRUE, bool $invalidateCache = TRUE) : void {
    $this->ensureEnabled();
    $update = $this->client->createUpdate();
    $cacheableMetadata = NULL;
    if ($invalidateCache) {
      $cacheableMetadata = new CacheableMetadata();
    }
    foreach ($entities as $entity) {
      $this->indexEntity($entity, TRUE, $update, $indexRelated, $cacheableMetadata);
    }
    if ($invalidateCache) {
      Cache::invalidateTags($cacheableMetadata->getCacheTags());
    }
    if ($commit) {
      $update->addCommit();
    }
    $this->client->update($update);
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $throwOnNonModel
   * @param \Solarium\QueryType\Update\Query\Query|NULL $update
   * @param bool $indexRelated
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   */
  public function indexEntity(ContentEntityInterface $entity, bool $throwOnNonModel = TRUE, Query $update = NULL, bool $indexRelated = TRUE, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $model = $this->getIndexableModelFromEntity($entity, $throwOnNonModel);
    if ($model instanceof SolrModelInterface) {
      $updateProvided = $update instanceof Query;
      if (!$updateProvided) {
        $update = $this->client->createUpdate();
      }
      $this->indexModel($model, $update, $cacheableMetadata);
      if ($indexRelated) {
        $relatedModels = $this->collectRelatedModels($model);
        $this->indexModels($relatedModels, $update, $cacheableMetadata);
      }
      if (!$updateProvided) {
        $update->addCommit();
        $this->client->update($update);
      }
    }
  }

  /**
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param bool $throwOnNonModel
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   */
  public function deleteEntity(ContentEntityInterface $entity, bool $throwOnNonModel = TRUE, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $model = $this->getIndexableModelFromEntity($entity, $throwOnNonModel);
    if ($model instanceof SolrModelInterface) {
      $update = $this->client->createUpdate();
      $this->deleteModel($model, $update, $cacheableMetadata);
      $relatedModels = $this->collectRelatedModels($model);
      $this->indexModels($relatedModels, $update, $cacheableMetadata);
      $update->addCommit();
      $this->client->update($update);
    }
  }

  /**
   * @param array $models
   * @param \Solarium\QueryType\Update\Query\Query|NULL $update
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   */
  public function indexModels(array $models, Query $update = NULL, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $updateProvided = $update instanceof Query;
    if (!$updateProvided) {
      $update = $this->client->createUpdate();
    }
    foreach ($models as $model) {
      $this->indexModel($model, $update, $cacheableMetadata);
    }
    if (!$updateProvided) {
      $update->addCommit();
      $this->client->update($update);
    }
  }

  /**
   * @param \Drupal\ctek_search\SolrModelInterface $model
   * @param \Solarium\QueryType\Update\Query\Query|NULL $update
   * @param \Drupal\Core\Cache\CacheableMetadata $cacheableMetadata
   */
  public function indexModel(SolrModelInterface $model, Query $update = NULL, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $updateProvided = $update instanceof Query;
    if (!$updateProvided) {
      $update = $this->client->createUpdate();
    }
    if ($model->shouldIndex()) {
      if ($cacheableMetadata) {
        $cacheableMetadata->addCacheableDependency($model);
      }
      $update->addDocument($model->getSolrDocument());
    } else {
      $update->addDeleteById($model->idForIndexing());
    }
    if (!$updateProvided) {
      $update->addCommit();
      $this->client->update($update);
    }
  }

  /**
   * @param array $models
   * @param \Solarium\QueryType\Update\Query\Query|NULL $update
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   */
  public function deleteModels(array $models, Query $update = NULL, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $updateProvided = $update instanceof Query;
    if (!$updateProvided) {
      $update = $this->client->createUpdate();
    }
    foreach ($models as $model) {
      $this->deleteModel($model, $update, $cacheableMetadata);
    }
    if (!$updateProvided) {
      $update->addCommit();
      $this->client->update($update);
    }
  }

  /**
   * @param \Drupal\ctek_search\SolrModelInterface $model
   * @param \Solarium\QueryType\Update\Query\Query|NULL $update
   * @param \Drupal\Core\Cache\CacheableMetadata|null $cacheableMetadata
   */
  public function deleteModel(SolrModelInterface $model, Query $update = NULL, CacheableMetadata $cacheableMetadata = NULL) : void {
    $this->ensureEnabled();
    $updateProvided = $update instanceof Query;
    if (!$updateProvided) {
      $update = $this->client->createUpdate();
    }
    if ($cacheableMetadata) {
      $cacheableMetadata->addCacheableDependency($model);
    }
    $update->addDeleteById($model->idForIndexing());
    if (!$updateProvided) {
      $update->addCommit();
      $this->client->update($update);
    }
  }

  /**
   *
   */
  public function commit() : void {
    $this->ensureEnabled();
    $update = $this->client->createUpdate();
    $update->addCommit();
    $this->client->update($update);
  }

}
