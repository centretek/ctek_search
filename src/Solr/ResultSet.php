<?php

namespace Drupal\ctek_search\Solr;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\ctek_search\Constants;
use Drupal\ctek_search\SolrModelInterface;
use Drupal\ctek_search\SolrModelPluginManager;
use Solarium\QueryType\Select\Result\Document;
use Solarium\QueryType\Select\Result\Result;

class ResultSet {

  protected $query;

  protected $result;

  protected $solrModelPluginManager;

  protected $entityTypeManager;

  protected $logger;

  protected static $storages = [];

  public function __construct(
    SelectQuery $query,
    Result $result,
    SolrModelPluginManager $solrModelPluginManager,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->query = $query;
    $this->result = $result;
    $this->solrModelPluginManager = $solrModelPluginManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  public function count() {
    return $this->result->count();
  }

  public function total() {
    return $this->result->getNumFound();
  }

  protected function getStorage(string $entityType) {
    if (!isset(static::$storages[$entityType])) {
      static::$storages[$entityType] = $this->entityTypeManager->getStorage($entityType);
    }
    return static::$storages[$entityType];
  }

  public function getRawResult() {
    return $this->result;
  }

  public function getRawQuery() {
    return $this->query;
  }

  public function getIds() {
    return array_map(function(Document $document){
      return $document->getFields()['entity_id_i'];
    }, $this->result->getDocuments());
  }

  /**
   * @return SolrModelInterface[]
   */
  public function getModels() {
    $models = [];
    /** @var Document $document */
    foreach ($this->result->getDocuments() as $document) {
      $fields = $document->getFields();
      if (isset($fields['entity_type_s']) && isset($fields['entity_id_i'])) {
        try {
          /** @var \Drupal\Core\Entity\EntityStorageInterface $storage */
          $storage = ResultSet::getStorage($fields['entity_type_s']);
          $entity = $storage->load($fields['entity_id_i']);
          if (!$entity instanceof ContentEntityInterface) {
            continue;
          }
          $model = $this->solrModelPluginManager->wrap($entity);
          if (!$model instanceof SolrModelInterface) {
            throw new \LogicException('Unable to locate SolrModelInterface for result entity.');
          }
          $models[$fields['id']] = $model;
        } catch (\Exception $e) {
          \Drupal::logger(Constants::LOGGER_CHANNEL)->error("@exception_message:\n@exception_trace", [
            '@exception_message' => $e->getMessage(),
            '@exception_trace' => $e->getTraceAsString(),
          ]);
        }
      }
    }
    return $models;
  }

}
