<?php

namespace Drupal\ctek_search\Solr;

use Symfony\Component\EventDispatcher\Event as LegacyEvent;
use Symfony\Contracts\EventDispatcher\Event;

class CompatibilityEvent extends LegacyEvent {

  protected $innerEvent;

  public function __construct(object $innerEvent) {
    $this->innerEvent = $innerEvent;
  }

  public function isPropagationStopped() {
    if ($this->innerEvent instanceof LegacyEvent || $this->innerEvent instanceof Event) {
      return $this->innerEvent->isPropagationStopped();
    }
    return parent::isPropagationStopped();
  }

  public function stopPropagation() {
    if ($this->innerEvent instanceof LegacyEvent || $this->innerEvent instanceof Event) {
      $this->innerEvent->stopPropagation();
      return;
    }
    parent::stopPropagation();
  }

}
