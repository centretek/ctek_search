<?php

namespace Drupal\ctek_search\Solr;

use Solarium\QueryType\Select\Query\Query as SolariumQuery;

class NodeModelSelectQuery extends SelectQuery {

  private $entityTypeSet = FALSE;

  public function __construct(SolariumQuery $query) {
    parent::__construct($query);
    $this->setEntityType('node');
    $this->query->addSort('title_s', SolariumQuery::SORT_ASC);
    $this->query->addField('title_txt_en_split');
  }

  public function setEntityType($entityType) {
    if ($this->entityTypeSet) {
      throw new \LogicException('NodeModelSelectQuery does not support setting the entity type.');
    }
    parent::setEntityType($entityType);
  }

}
