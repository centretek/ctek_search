<?php

namespace Drupal\ctek_search\Solr;
use Solarium\Component\Result\Facet\Field;
use Solarium\QueryType\Select\Query\Query as BaseSelectQuery;

class GlossaryQuery extends SelectQuery {

  public function __construct(BaseSelectQuery $query) {
    parent::__construct($query);
    $this->query->setRows(0);
    $options = [
      'key' => 'glossary',
      'local_key' => 'glossary',
      'local_exclude' => 'letter'
    ];
    $query
      ->getFacetSet()
      ->setMinCount(1)
      ->createFacetField($options)
      ->setExcludeTerms($options['key'])
      ->setField('glossary_s');
  }

  public static function getLetters(ResultSet $resultSet) {
    $facetLetters = [];
    $facetField = $resultSet
      ->getRawResult()
      ->getFacetSet()
      ->getFacet('glossary');
    if ($facetField instanceof Field) {
      $facetLetters = $facetField->getValues();
    }
    $letters = [];
    $start = ord('A');
    for ($i = 0; $i < 26; $i++) {
      $letter = chr($start + $i);
      $letters[$letter] = isset($facetLetters[$letter]) ? $facetLetters[$letter] : NULL;
    }
    return $letters;
  }

  public function setLetter(string $letter) {
    $this
      ->getSolrQuery()
      ->createFilterQuery('letter')
      ->addTag('letter')
      ->setQuery('glossary_s:%T1%', [$letter]);
  }

}
