<?php

namespace Drupal\ctek_search\Solr;

use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;

class CompatibilityEventDispatcher implements EventDispatcherInterface {

  protected $eventDispatcher;

  public function __construct() {
    $this->eventDispatcher = new EventDispatcher();
  }

  public function dispatch(object $event) {
    $wrapper = new CompatibilityEvent($event);
    $this->eventDispatcher->dispatch((new \ReflectionClass($event))->getShortName(), $wrapper);
  }

}
