<?php

namespace Drupal\ctek_search\Solr;

use Solarium\Core\Query\Helper;
use Solarium\Core\Query\AbstractQuery as SolariumQuery;

abstract class AbstractQuery {

  /**
   * @var SolariumQuery
   */
  protected $query;

  /**
   * @var \Solarium\Core\Query\Helper
   */
  protected $helper;

  public function __construct(SolariumQuery $query) {
    $this->query = $query;
    $this->helper = new Helper();
  }

  public function preExecute() {

  }

  /**
   * @return SolariumQuery
   */
  public function getSolrQuery() {
    return $this->query;
  }
}
