<?php

namespace Drupal\ctek_search\Solr;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\ctek_search\SolrModelPluginManager;

class Searcher {

  const INVALID_ARGUMENTS = 'Argument must be either a fully qualified class name or a callable with a single argument that extends from AbstractQuery.';

  protected $client;

  protected $solrModelPluginManager;

  protected $entityTypeManager;

  public function __construct(
    Client $client,
    SolrModelPluginManager $solrModelPluginManager,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->client = $client;
    $this->solrModelPluginManager = $solrModelPluginManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param $handler string|callable
   *
   * @return \Drupal\ctek_search\Solr\ResultSet
   * @throws \ReflectionException
   */
  public function search($handler) {
    if (is_callable($handler)) {
      $reflect = new \ReflectionFunction($handler);
      $parameters = $reflect->getParameters();
      if (count($parameters) === 0) {
        throw new \InvalidArgumentException(static::INVALID_ARGUMENTS);
      }
      $class = $parameters[0]->getType() && !$parameters[0]->getType()->isBuiltin() 
        ? new \ReflectionClass($parameters[0]->getType()->getName())
        : null;
    } elseif (is_string($handler)) {
      $class = new \ReflectionClass($handler);
    } else {
      throw new \InvalidArgumentException(static::INVALID_ARGUMENTS);
    }
    if (!$class->isSubclassOf(AbstractQuery::class)) {
      throw new \InvalidArgumentException(static::INVALID_ARGUMENTS);
    }
    $solariumQuery = $this->client->createSelect();
    /**
     * @var $query SelectQuery
     */
    $query = $class->newInstance($solariumQuery);
    if (is_callable($handler)) {
      $handler($query);
    }
    $query->preExecute();
    $response = $this->client->select($solariumQuery);
    return new ResultSet(
      $query,
      $response,
      $this->solrModelPluginManager,
      $this->entityTypeManager
    );
  }

}
