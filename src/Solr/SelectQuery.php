<?php

namespace Drupal\ctek_search\Solr;

use Solarium\QueryType\Select\Query\Query as BaseSelectQuery;

class SelectQuery extends AbstractQuery {

  /**
   * @var BaseSelectQuery
   */
  protected $query;

  /**
   * @return BaseSelectQuery
   */
  public function getSolrQuery() {
    /** @var BaseSelectQuery $query */
    $query = parent::getSolrQuery();
    return $query;
  }

  public function __construct(BaseSelectQuery $query) {
    parent::__construct($query);
    $this->query->setHandler('elevate');
    $this->query->setQuery('*:*');
    $site = $this->query->createFilterQuery('site');
    $site->setQuery('site_s:%T1%', [$_ENV['CTEK_SITE_NAME']]);
    $env = $this->query->createFilterQuery('env');
    $env->setQuery('environment_s:%T1%', [$_ENV['CTEK_ENV']]);
    $this->query->addFilterQuery($site);
    $this->query->setFields([
      'id',
      'entity_id_i',
      'entity_type_s',
      'bundle_s',
    ]);
  }

  /**
   * @param $resultsPerPage int
   * @param $page int Zero-based
   *
   * @return \Drupal\ctek_search\Solr\SelectQuery
   */
  public function paginate($resultsPerPage, $page) {
    $this->query->setRows($resultsPerPage);
    $this->query->setStart($resultsPerPage * $page);
    return $this;
  }

  public function setEntityType($entityType) {
    $this->query
      ->createFilterQuery('entity')
      ->setQuery('entity_type_s:%T1%', [$entityType]);
    return $this;
  }

  public function setBundle($bundle) {
    $this->query
      ->createFilterQuery('bundle')
      ->setQuery('bundle_s:%T1%', [$bundle]);
    return $this;
  }

  public function createMultiValueCondition($name, $field, $values) {
    $count = 0;
    $placeholders = array_map(function() use (&$count) {
      $count++;
      return "%T$count%";
    }, $values);
    $this
      ->getSolrQuery()
      ->createFilterQuery($name)
      ->addTag($name)
      ->setQuery($field . ':(' . join(' OR ', $placeholders) . ')', array_values($values));
  }

}
