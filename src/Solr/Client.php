<?php

namespace Drupal\ctek_search\Solr;

use Psr\EventDispatcher\EventDispatcherInterface;
use Solarium\Core\Client\Adapter\AdapterInterface;
use Solarium\Core\Client\Adapter\Curl;
use Symfony\Component\EventDispatcher\EventDispatcher;

class Client extends \Solarium\Client {

  public function __construct(AdapterInterface $adapter, EventDispatcherInterface $eventDispatcher) {
    parent::__construct($adapter, $eventDispatcher, [
      'endpoint' => [
        'search' => [
          'host' => $_ENV['SOLR_HOST'],
          'port' => $_ENV['SOLR_PORT'],
          'path' => $_ENV['SOLR_PATH'],
          'core' => $_ENV['SOLR_CORE'],
          'username' => $_ENV['SOLR_USER'],
          'password' => $_ENV['SOLR_PASSWORD'],
        ],
      ],
    ]);
  }

}
