<?php

namespace Drupal\ctek_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ctek_search\BatchProcess;
use Drupal\ctek_search\Solr\Client;
use Drupal\ctek_search\Solr\Indexer;
use Drupal\ctek_search\SolrModelPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DashboardForm extends FormBase {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ctek_search.client'),
      $container->get('ctek_search.indexer'),
      $container->get('plugin.manager.solr-model')
    );
  }

  protected $client;

  protected $indexer;

  protected $solrModelPluginManager;

  public function __construct(
    Client $client,
    Indexer $indexer,
    SolrModelPluginManager $solrModelPluginManager
  ) {
    $this->client = $client;
    $this->indexer = $indexer;
    $this->solrModelPluginManager = $solrModelPluginManager;
  }

  public function getFormId() {
    return 'ctek_search_dashboard_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $ping = $this->client->createPing();
    $start = microtime(TRUE);
    try {
      $this->client->ping($ping);
      $time = microtime(TRUE) - $start;
    } catch (\Exception $e) {
      $time = $this->t('Unable to reach solr server.');
    }
    $endpoint = $this->client->getEndpoint();
    $form['stats'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Stats'),
    ];
    $form['stats'][] = [
      '#type' => 'table',
      '#rows' => [
        [
          $this->t('Scheme'),
          $endpoint->getScheme(),
        ],
        [
          $this->t('Host'),
          $endpoint->getHost(),
        ],
        [
          $this->t('Port'),
          $endpoint->getPort(),
        ],
        [
          $this->t('Path'),
          $endpoint->getPath(),
        ],
        [
          $this->t('Ping'),
          number_format($time * 1000, 0) . 'ms',
        ],
      ],
    ];
    $form['indices'] = [
      '#type' => 'table',
    ];
    $definitions = $this->solrModelPluginManager->getDefinitions();
    usort($definitions, function($a, $b){
      if ($a['id'] === $b['id']) {
        return 0;
      }
      return ($a['id'] < $b['id']) ? -1 : 1;
    });
    foreach ($definitions as $definition) {
      $count = $this->indexer->getDbCountForIndex($definition);
      $form['indices'][] = [
        [
          '#markup' => $this->t('Index: @id', [
            '@id' => $definition['id'],
          ]),
        ],
        [
          '#markup' => $this->t('@count @items', [
            '@count' => $count,
            '@items' => $this->formatPlural($count, 'item', 'items'),
          ]),
        ],
        [
          '#type' => 'submit',
          '#value' => $this->t('Reindex'),
          '#name' => $definition['id'],
          '#solr-index' => $definition['id'],
        ],
      ];
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['all'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reindex All'),
      '#name' => 'all',
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($form_state->getTriggeringElement()['#name']) {
      case 'all':
        BatchProcess::process([
          BatchProcess::CONFIG_KEY_DEFINITIONS => $this->solrModelPluginManager->getDefinitions()
        ]);
        break;
      default:
        $indexId = $form_state->getTriggeringElement()['#solr-index'];
        BatchProcess::process([
          BatchProcess::CONFIG_KEY_DEFINITIONS => [$this->solrModelPluginManager->getDefinition($indexId)],
        ]);
        break;
    }
  }

}
