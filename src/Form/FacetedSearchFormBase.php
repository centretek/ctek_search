<?php

namespace Drupal\ctek_search\Form;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\ctek_common\Form\UrlParameterFormBase;
use Drupal\ctek_search\Solr\Client;
use Drupal\ctek_search\Solr\NodeModelSelectQuery;
use Drupal\node\NodeInterface;
use Solarium\Component\Result\Facet\Field;
use Solarium\QueryType\Select\Query\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class FacetedSearchFormBase extends UrlParameterFormBase {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ctek_search.client'),
      $container->get('entity_type.manager')
    );
  }

  protected $client;

  protected $request;

  protected $query;

  protected $facetSet;

  protected $termStorage;

  protected $formKeys = [];

  public function __construct(
    Client $client,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->client = $client;
    $this->request = $this->getRequest();
    $this->query = new NodeModelSelectQuery(new Query());
    $this->termStorage = $entityTypeManager->getStorage('taxonomy_term');
  }

  public function getParametersForRoute() {
    return $this->formKeys;
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#process'][] = '::processFacets';
    $form['#process'][] = '::process';
    return $form;
  }

  protected function addFacet($options) {
    $options['local_exclude'] = $options['local_key'];
    $this->query
      ->getSolrQuery()
      ->getFacetSet()
      ->createFacetField($options);
  }

  protected function addFilterQuery($key, $field, $values, $conjunction = 'AND') {
    $placeholders = [];
    if (is_array($values)) {
      for ($i = 0;$i < count($values); $i++) {
        $placeholders[] = '%P' . ($i + 1) . '%';
      }
      $placeholders = '(' . join(' ' . $conjunction . ' ', $placeholders) . ')';
    } else {
      $values = [$values];
      $placeholders = '%P1%';
    }
    $this->query
      ->getSolrQuery()
      ->createFilterQuery($key)
      ->addTag($key)
      ->setQuery($field . ':' . $placeholders, array_values($values));
  }

  protected function getFacetSet() {
    if (!$this->facetSet) {
      $solrQuery = $this->query->getSolrQuery();
      $solrQuery->setRows(0);
      $results = $this->client->select($solrQuery);
      $this->facetSet = $results->getFacetSet();
    }
    return $this->facetSet;
  }

  public function processFacets($element, FormStateInterface $formState, &$form) {
    foreach (Element::children($element) as $key) {
      if (isset($element[$key]['#facet'])) {
        if (!isset($element[$key]['#facet']['local_key'])) {
          $element[$key]['#facet']['local_key'] = $key;
        }
        if ($element[$key]['#type'] !== 'glossary' && !isset($element[$key]['#facet']['mincount'])) {
          $element[$key]['#facet']['mincount'] = 0;
        }
        $this->addFacet($element[$key]['#facet']);
      }
      $values = $this->request->get($key);
      if (isset($element[$key]['#solr_field'])) {
        if (isset($element[$key]['#solr_filter_callback']) && is_callable($element[$key]['#solr_filter_callback'])) {
          $element[$key]['#solr_filter_callback']($key, $element[$key]['#solr_field'], $values);
        } elseif ($values) {
          $conjunction = 'AND';
          if (isset($element[$key]['#facet_conjunction'])) {
            $conjunction = $element[$key]['#facet_conjunction'];
          }
          $this->addFilterQuery($key, $element[$key]['#solr_field'], $values, $conjunction);
        }
      }
      $element[$key] = $this->processFacets($element[$key], $formState, $form);
    }
    return $element;
  }

  public function process($element, FormStateInterface $formState, &$form, $parents = [], &$clear = NULL) {
    $facetSet = $this->getFacetSet();
    if (!$clear) {
      $clear =& $element['clear'];
    }
    foreach (Element::children($element) as $key) {
      $localParents = $parents;
      if (!isset($element[$key]['#type']) || !in_array($element[$key]['#type'], [
          'textfield',
          'glossary',
          'checkbox',
          'checkboxes',
          'select',
          'hidden',
          'container',
        ])) {
        continue;
      }
      $type = $element[$key]['#type'];
      if ($type === 'hidden' && in_array($key, [
          'form_build_id',
          'form_id',
        ])) {
        continue;
      }
      if (isset($element[$key]['#facet'])) {
        $facets = $facetSet->getFacet($element[$key]['#facet']['local_key']);
        if ($facets instanceof Field) {
          $facets = iterator_to_array($facets);
          $counts = array_values($facets);
          if (isset($element[$key]['#title'])) {
            $element[$key]['#raw_title'] = $element[$key]['#title'];
          }
          if (isset($element[$key]['#attributes']['data-title'])) {
            $element[$key]['#raw_title'] = $element[$key]['#attributes']['data-title'];
          }

          if (isset($element[$key]['#mapping'])) {
            if (!isset($element[$key]['#options']) || count($element[$key]['#options']) === 0) {
              $element[$key]['#options'] = [];
            }
            $element[$key]['#options'] += $this->mapFacets($element[$key]['#mapping']['entityTypeId'], $element[$key]['#mapping']['bundle'], $facets);
          } elseif ($type === 'checkbox') {
            $element[$key]['#raw_options'] = [
              1 => isset($element[$key]['#checked_label']) ? $element[$key]['#checked_label'] : $this->t('Yes'),
            ];
          } elseif (!isset($element[$key]['#options'])) {
            $options = array_combine(array_keys($facets), array_keys($facets));
            $element[$key]['#options'] = $options;
          } else {
            $hasOptions = FALSE;
            foreach ($element[$key]['#options'] as $value => $label) {
              if ($value) {
                $hasOptions = TRUE;
              }
            }
            if (!$hasOptions) {
              $options = array_combine(array_keys($facets), array_keys($facets));
              $element[$key]['#options'] += $options;
            }
          }

          if (isset($element[$key]['#options'])) {
            $element[$key]['#raw_options'] = $element[$key]['#options'];
          }
          if (isset($element[$key]['#show_count']) && $element[$key]['#show_count']) {
            if (isset($element[$key]['#options'])) {
              $element[$key]['#options'] = array_combine(array_keys($element[$key]['#options']), array_map(function ($option, $count) {
                return $option . ' (' . $count . ')';
              }, $element[$key]['#options'], $counts));
              $element[$key]['#options_attributes'] = array_filter(array_combine(array_keys($element[$key]['#options']), array_map(function ($option, $count) {
                return $count === 0 ? ['disabled' => TRUE] : NULL;
              }, $element[$key]['#options'], $counts)));
            } elseif ($type === 'checkbox' && isset($facets['true'])) {
              $element[$key]['#title'] .= ' (' . $facets['true'] . ')';
              if ($facets['true'] === 0) {
                $element[$key]['#disabled'] = TRUE;
              }
            }
          }
          switch ($type) {
            case 'select':
              $element[$key]['#attributes']['onChange'] = 'jQuery("#edit-search").click()';
              $element[$key]['#validated'] = TRUE;
              if (isset($element[$key]['#option_callback'])) {
                $element[$key]['#options_attributes'] = array_filter(array_combine(array_keys($element[$key]['#options']), array_map(
                  $element[$key]['#option_callback'],
                  $element[$key]['#raw_options'],
                  $element[$key]['#options_attributes']
                )));
              }
              asort($element[$key]['#options']);
              break;
            case 'glossary':
              $letters = [];
              $start = ord('a');
              for ($i = 0; $i < 26; $i++) {
                $letter = chr($start + $i);
                $letters[$letter] = isset($facets[$letter]) ? $facets[$letter] : NULL;
              }
              $element[$key]['#letters'] = $letters;
              break;
          }
        }
      }
      if (!isset($element[$key]['#default_value'])) {
        $element[$key]['#default_value'] = $this->request->get($key, isset($element[$key]['#options']) ? [] : NULL);
      }
      if ($type === 'checkboxes' && isset($element[$key]['#exclusive'])) {
        $element[$key]['#after_build'][] = [$this, 'handleExclusive'];
      }
      $this->formKeys[] = [
        'parents' => $parents,
        'key' => $key,
      ];
      $values = $this->request->get($key);
      if ($values !== NULL) {
        if (!is_array($values)) {
          $values = [$values];
        }

        $optionsKey = isset($element[$key]['#raw_options']) ? '#raw_options' : '#options';
        foreach ($values as $value) {
          if (isset($element[$key]['#clear_label'])) {
            if (is_callable($element[$key]['#clear_label'])) {
              $label = call_user_func_array($element[$key]['#clear_label'], [$element[$key], $value]);
            } else {
              //              $label = $element[$key]['#clear-label'] . ': ' . (isset($element[$key][$optionsKey][$value]) ? $element[$key][$optionsKey][$value] : $value);
              $label = $element[$key]['#clear_label'];
            }
          } elseif (isset($element[$key]['#raw_title'])) {
            //            $label = $element[$key]['#raw_title'] . ': ' . (isset($element[$key][$optionsKey][$value]) ? $element[$key][$optionsKey][$value] : $value);
            $label = (isset($element[$key][$optionsKey][$value]) ? $element[$key][$optionsKey][$value] : $value);
          } else {
            continue;
          }
          $clearKey = 'clear-' . $key . '-' . $value;
          $clear[$clearKey] = [
            '#type' => 'submit',
            '#name' => Crypt::hashBase64($clearKey),
            '#clear-key' => $key,
            '#clear-value' => $value,
            '#value' => $label,
            '#prefix' => '<div class="filter-item"><div class="filter-wrap"><span>close icon</span>',
            '#suffix' => '</div></div>',
          ];
        }
      }
      $localParents[] = $key;
      $element[$key] = $this->process($element[$key], $formState, $form, $localParents, $clear);
    }
    return $element;
  }

  public function handleExclusive($element, FormStateInterface $formState) {
    if ($element['#value']) {
      foreach (Element::children($element) as $key) {
        if ((string)$key !== reset($element['#value'])) {
          $element[$key]['#attributes']['disabled'] = TRUE;
        }
      }
    }
    return $element;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] === 'clear') {
      $form_state->setRedirect($this->getRouteNameForRedirect($form_state));
      return;
    }
    $triggeringElement = $form_state->getTriggeringElement();
    $allParams = iterator_to_array($this->request->query);
    $routeParams = $this->getRouteValuesForRedirect($form, $form_state, $allParams);
    if (isset($routeParams['page'])) {
      unset($routeParams['page']);
    }
    if (isset($triggeringElement['#clear-key']) && isset($routeParams[$triggeringElement['#clear-key']])) {
      if (is_array($routeParams[$triggeringElement['#clear-key']])) {
        $routeParams[$triggeringElement['#clear-key']] = array_diff(
          $routeParams[$triggeringElement['#clear-key']],
          [$triggeringElement['#clear-value']]
        );
      } else {
        unset($routeParams[$triggeringElement['#clear-key']]);
      }
    }
    $form_state->setRedirect(\Drupal::routeMatch()->getRouteName(), $routeParams);
  }

  protected function mapFacets($entityTypeId, $bundle, $facets, callable $queryCallback = NULL, callable $nameCallback = NULL) {
    $options = [];
    $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
    $query = $storage->getQuery();
    if ($bundle) {
      $bundleKey = \Drupal::entityTypeManager()->getDefinition($entityTypeId)->getKey('bundle');
      $query->condition($bundleKey, $bundle);
    }
    if ($entityTypeId === 'node') {
      $query->condition('status', NodeInterface::PUBLISHED);
    }
    if (is_callable($queryCallback)) {
      $queryCallback($query);
    }
    $items = [];
    foreach ($storage->loadMultiple($query->execute()) as $item) {
      $items[$item->id()] = is_callable($nameCallback) ? $nameCallback($item) : $item->label();
    }
    foreach ($facets as $facet => $count) {
      if (isset($items[$facet])) {
        $options[$facet] = $items[$facet];
      }
    }
    return $options;
  }

}
