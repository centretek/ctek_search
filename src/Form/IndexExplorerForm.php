<?php

namespace Drupal\ctek_search\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\ctek_common\Form\UrlParameterFormBase;
use Drupal\ctek_search\Solr\Client;
use Drupal\ctek_search\Solr\Searcher;
use Drupal\ctek_search\Solr\SelectQuery;
use Drupal\ctek_search\SolrModelPluginManager;
use Solarium\Exception\HttpException;
use Solarium\QueryType\Select\Result\Document;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IndexExplorerForm extends UrlParameterFormBase {

  const PAGE_SIZE = 20;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ctek_search.client'),
      $container->get('ctek_search.searcher'),
      $container->get('plugin.manager.solr-model')
    );
  }

  protected $client;

  protected $searcher;

  protected $solrModelPluginManager;

  public function __construct(
    Client $client,
    Searcher $searcher,
    SolrModelPluginManager $solrModelPluginManager
  ) {
    $this->client = $client;
    $this->searcher = $searcher;
    $this->solrModelPluginManager = $solrModelPluginManager;
  }

  public function getParametersForRoute() {
    return [
      'page_size',
      'page',
      'definitions',
      'query',
      'fields',
      'sort',
    ];
  }

  public function getFormId() {
    return 'ctek_search_index_explorer_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $ping = $this->client->createPing(['handler' => 'admin/luke']);
    $results = $this->client->ping($ping);
    $luke = $results->getData();
//    $fields = [
//      '*' => $this->t('All'),
//    ];
//    foreach ($luke['fields'] as $name => $field) {
//      $fields[$name] = "$name ({$field['type']})";
//    }
//    $form['fields'] = [
//      '#type' => 'checkboxes',
//      '#multiple' => TRUE,
//      '#title' => $this->t('Fields'),
//      '#options' => $fields,
//      '#default_value' => $request->get('fields', ['*']),
//    ];
    $form['#attached']['library'][] = 'ctek_search/index-explorer';
    $form['left'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search'),
      '#attributes' => [
        'class' => [

        ],
      ],
    ];
    $definitions = [];
    foreach ($this->solrModelPluginManager->getDefinitions() as $definition) {
      $definitions[$definition['entityType'] . ':' . $definition['bundle']] = $definition['entityType'] . ' -> ' . $definition['bundle'];
    }
    asort($definitions);
    $form['left']['definitions'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#multiple' => TRUE,
      '#options' => $definitions,
      '#default_value' => $request->get('definitions'),
    ];
    $form['left']['query'] = [
      '#type' => 'textarea',
      '#rows' => 3,
      '#cols' => 10,
      '#title' => $this->t('Query'),
      '#description' => $this->t('Uses Lucene syntax.'),
      '#default_value' => $request->get('query'),
    ];
    $sorts = [];
    foreach ($luke['fields'] as $name => $field) {
      $sorts[$name . ':' . 'asc'] = "$name (Asc)";
      $sorts[$name . ':' . 'desc'] = "$name (Desc)";
    }
    $form['left']['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#options' => $sorts,
      '#empty_option' => $this->t('Default (Entity, Bundle, Name/Title)'),
      '#default_value' => $request->get('sort'),
    ];
    $form['left']['page_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Page Size'),
      '#options' => [
        20 => 20,
        50 => 50,
        100 => 100,
        1000 => 1000,
      ],
      '#default_value' => $request->get('page_size', static::PAGE_SIZE),
    ];
    $form['left']['actions'] = [
      '#type' => 'actions',
      'search' => [
        '#type' => 'submit',
        '#value' => $this->t('Search'),
      ]
    ];
    $form['right'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Results'),
      '#attributes' => [
        'class' => [

        ],
      ],
    ];
    $form['right']['pager1'] = [
      '#type' => 'pager',
    ];
    try {
      $form['right']['results'] = $this->getResults();
    } catch (HttpException $e) {
      $form['right']['results'] = ['#markup' => $this->t('No Results.')];
      $body = Json::decode($e->getBody());
      if (isset($body['error']['msg'])) {
        \Drupal::messenger()->addError($body['error']['msg']);
      } else {
        \Drupal::messenger()->addError($e->getMessage());
      }
    }
    $form['right']['pager2'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

  protected function getResults() {
    $request = \Drupal::request();
    $pageSize = $request->query->get('page_size', static::PAGE_SIZE);
    $page = $request->query->get('page', 0);
    $definitions = $request->get('definitions', []);
    $luceneQuery = $request->get('query');
    $sort = $request->get('sort');
    $results = $this->searcher->search(function(SelectQuery $query) use (
      $pageSize,
      $page,
      $definitions,
      $luceneQuery,
      $sort
    ) {
      $query
        ->getSolrQuery()
        ->clearSorts()
        ->addSort('entity_type_s', 'asc')
        ->addSort('bundle_s', 'asc');
      if ($sort) {
        $query
          ->getSolrQuery()
          ->addSort(...explode(':', $sort));
      } else {
        $query
          ->getSolrQuery()
          ->addSort('name_s', 'asc')
          ->addSort('title_s', 'asc');
      }
      $query
        ->getSolrQuery()
        ->setFields([
          'id',
          'title_s',
          'name_s',
          'entity_type_s',
          'bundle_s',
        ]);
      if (count($definitions) > 0) {
        $entityTypes = [];
        $bundles = [];
        foreach ($definitions as $definition) {
          [$entityType, $bundle] = explode(':', $definition);
          $entityTypes[] = "(entity_type_s: $entityType)";
          $bundles[] = "(bundle_s: $bundle)";
        }
        $entityTypes = array_unique($entityTypes);
        $bundles = array_unique($bundles);
        $query
          ->getSolrQuery()
          ->createFilterQuery('entity_type')
          ->setQuery(join(' OR ', $entityTypes));
        $query
          ->getSolrQuery()
          ->createFilterQuery('bundle')
          ->setQuery(join(' OR ', $bundles));
      }
      if ($luceneQuery) {
        $query
          ->getSolrQuery()
          ->setQuery($luceneQuery);
      }
      $query->paginate(
        $pageSize,
        $page
      );
    });
    /** @var \Drupal\Core\Pager\PagerManagerInterface $pagerManager */
    $pagerManager = \Drupal::service('pager.manager');
    $pagerManager->createPager($results->total(), $pageSize);
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name/Title'),
        $this->t('Entity Type'),
        $this->t('Bundle'),
      ],
      '#rows' => [],
    ];
    /** @var Document $result */
    foreach ($results->getRawResult()->getDocuments() as $result) {
//      $build[] = $this->formatResult($result);
      $fields = $result->getFields();
      $table['#rows'][] = [
        [
          'data' => [
            '#type' => 'link',
            '#title' => $fields['title_s'] ?? $fields['name_s'] ?? '[NO TITLE/NAME]',
            '#url' => Url::fromRoute('ctek_search.index-explorer.details', ['id' => $fields['id']]),
            '#attributes' => [
              'class' => ['use-ajax'],
              'data-dialog-renderer' => 'off_canvas',
              'data-dialog-type' => 'dialog',
//              'data-dialog-type' => 'modal',
              'data-dialog-options' => Json::encode([
                'width' => '600',
              ]),
            ],
          ],
        ],
        $fields['entity_type_s'],
        $fields['bundle_s'],
      ];
    }
    return $table;
  }

  protected function formatResult(Document $result) {
    $fields = $result->getFields();
    $fieldPairs = [];
    foreach ($fields as $key => $value) {
      $fieldPairs[] = [
        $key,
        $value,
      ];
    }
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field'),
        $this->t('Value'),
      ],
      '#rows' => $fieldPairs,
    ];
    return $table;
  }

}
