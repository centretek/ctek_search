<?php

namespace Drupal\ctek_search\Commands;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ctek_search\BatchProcess;
use Drupal\ctek_search\Solr\Indexer;
use Drupal\ctek_search\SolrModelPluginManager;
use Drush\Commands\DrushCommands;

class IndexCommand extends DrushCommands {
  use StringTranslationTrait;

  /** @var Indexer */
  protected static $indexer;

  /** @var SolrModelPluginManager */
  protected static $solrModelPluginManager;

  /**
   * IndexCommand constructor.
   *
   * @param \Drupal\ctek_search\Solr\Indexer $indexer
   * @param \Drupal\ctek_search\SolrModelPluginManager $solrModelPluginManager
   */
  public function __construct(
    Indexer $indexer,
    SolrModelPluginManager $solrModelPluginManager
  ) {
    parent::__construct();
    static::$indexer = $indexer;
    static::$solrModelPluginManager = $solrModelPluginManager;
  }

  /**
   * @command ctek_search:index
   */
  public function index() {
    BatchProcess::process([
      BatchProcess::CONFIG_KEY_DEFINITIONS => static::$solrModelPluginManager->getDefinitions()
    ]);
  }

}
