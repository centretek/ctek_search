<?php

namespace Drupal\ctek_search;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\ctek_search\Annotation\SolrModel;

class SolrModelPluginManager extends DefaultPluginManager {

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Model',
      $namespaces,
      $module_handler,
      SolrModelInterface::class,
      SolrModel::class
    );

    $this->alterInfo('solr_model_info');
    $this->setCacheBackend($cache_backend, 'ctek_search');
  }

  public function wrap(ContentEntityInterface $entity) : ?SolrModelInterface {
    foreach ($this->getDefinitions() as $id => $definition) {
      if ($definition['entityType'] === $entity->getEntityTypeId() && $definition['bundle'] === $entity->bundle()) {
        return $this->createInstance($id, ['entity' => $entity]);
      }
    }
    return NULL;
  }

}
