<?php

namespace Drupal\ctek_search\Annotation;

/**
 * @Annotation
 */
class SolrField {

  public function __construct($options) {
    if (isset($options['value'])) {
      $this->name = $options['value'];
      unset($options['value']);
    }
    foreach ($options as $key => $value) {
      if (!property_exists($this, $key)) {
        throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
      }

      $this->$key = $value;
    }
  }

//  /**
//   * @var SerializerInterface
//   */
//  public $formatter;
//
//  public $boost;
//
//  public $skipIfNull = FALSE;

}
