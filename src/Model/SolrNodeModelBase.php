<?php

namespace Drupal\ctek_search\Model;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\ctek_common\Menu\MenuHelper;
use Drupal\ctek_common\Model\NodeModelBase;
use Drupal\ctek_search\Solr\Indexer;
use Drupal\ctek_search\SolrDocumentPluginManager;
use Drupal\ctek_search\SolrModelInterface;
use Drupal\ctek_search\SolrModelPluginManager;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\ctek_search\Annotation\SolrField;
use Drupal\pathauto\AliasCleanerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class SolrNodeModelBase extends NodeModelBase implements SolrModelInterface {
  use AnnotatedIndexableModelTrait;

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.solr-model'),
      $container->get('pathauto.alias_cleaner'),
      $container->get('ctek_common.menu_helper'));
  }

  protected $solrModelPluginManager;

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    SolrModelPluginManager $solrModelPluginManager,
    AliasCleanerInterface $aliasCleaner,
    MenuHelper $menuHelper
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $aliasCleaner,
      $menuHelper
    );
    $this->solrModelPluginManager = $solrModelPluginManager;
  }

  /**
   * @inheritDoc
   */
  public function getNode() : NodeInterface {
    return parent::getNode();
  }

  /**
   * @SolrField("title_txt_en_split")
   * @SolrField("title_txt_en_edgy")
   * @SolrField("title_s")
   * @inheritDoc
   */
  public function getTitle() : string {
    return parent::getTitle();
  }

  /**
   * @SolrField("created_i")
   * @inheritDoc
   */
  public function created() : int {
    return parent::created();
  }

  /**
   * @SolrField("changed_i")
   * @inheritDoc
   */
  public function changed() : int {
    return parent::changed();
  }

  /**
   * @SolrField("is_promoted_b")
   * @inheritDoc
   */
  public function isPromoted() : bool {
    return parent::isPromoted();
  }

  /**
   * @SolrField("is_sticky_b")
   * @inheritDoc
   */
  public function isSticky() : bool {
    return parent::isSticky();
  }

  /**
   * @inheritDoc
   */
  public function shouldIndex() : bool {
    return $this->getNode()->isPublished();
  }

  /**
   * @inheritDoc
   */
  public static function alterIndexQuery(QueryInterface $countQuery): void {
    $countQuery->condition('status', Node::PUBLISHED);
  }

  /**
   * @inheritDoc
   */
  public function getRelated(): array {
    return [];
  }

  public function getCacheTags(): array {
    return Cache::mergeTags([Indexer::getIndexCacheId($this->pluginDefinition)], parent::getCacheTags());
  }

}
