<?php

namespace Drupal\ctek_search\Model;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Drupal\Component\Annotation\Doctrine\DocParser;
use Drupal\Component\Annotation\Doctrine\SimpleAnnotationReader;
use Drupal\ctek_search\Annotation\SolrField;
use Solarium\QueryType\Update\Query\Document;

trait AnnotatedIndexableModelTrait {

  /**
   * @SolrField("entity_id_i")
   * @inheritDoc
   */
  public function getId() : int {
    return parent::getId();
  }

  /**
   * @SolrField("entity_type_s")
   * @inheritDoc
   */
  public function getEntityTypeId(): string {
    return parent::getEntityTypeId();
  }

  /**
   * @SolrField("bundle_s")
   * @inheritDoc
   */
  public function getBundle(): string {
    return parent::getBundle();
  }

  /**
   * @SolrField("site_s")
   * @return string
   */
  public function site() : string {
    return $_ENV['CTEK_SITE_NAME'];
  }

  /**
   * @SolrField("environment_s")
   * @return string
   */
  public function environment() : string {
    return $_ENV['CTEK_ENV'];
  }

  /**
   * @SolrField("id")
   * @return string
   */
  public function idForIndexing() : string {
    return join('-', [
      $this->site(),
      $this->environment(),
      $this->getEntityTypeId(),
      $this->getBundle(),
      $this->getId(),
    ]);
  }

  /**
   * @return \Solarium\QueryType\Update\Query\Document
   */
  public function getSolrDocument() : Document {
    $map = static::map();
    $fields = [];
    foreach ($map as $method => $solrFieldAnnotations) {
      /** @var SolrField $solrFieldAnnotation */
      foreach ($solrFieldAnnotations as $solrFieldAnnotation) {
        $fields[$solrFieldAnnotation->name] = $this->{$method}();
      }
    }
    ksort($fields);
    $document = new Document($fields);
    return $document;
  }

  protected static $map = [];

  protected static function map() {
    if (!isset(static::$map[static::class])) {
      static::$map[static::class] = [];
      $reflectionClass = new \ReflectionClass(static::class);
      $reader = new SimpleAnnotationReader();
      $reader->addNamespace('\Drupal\ctek_search\Annotation');
      AnnotationRegistry::registerLoader('class_exists');
      foreach ($reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC & ~\ReflectionMethod::IS_STATIC) as $method) {
        $solrFieldAnnotations = $reader->getMethodAnnotations($method);
        if (count($solrFieldAnnotations) > 0) {
          static::$map[static::class][$method->getName()] = $solrFieldAnnotations;
        }
      }
      AnnotationRegistry::reset();
    }
    return static::$map[static::class];
  }

}
