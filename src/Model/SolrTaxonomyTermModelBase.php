<?php

namespace Drupal\ctek_search\Model;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ctek_search\Annotation\SolrField;
use Drupal\ctek_search\SolrModelPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class SolrTaxonomyTermModelBase extends SolrModelBase implements ContainerFactoryPluginInterface {

  protected static $entityStorages = [];

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.solr-model'),
      $container->get('entity_type.manager')
    );
  }

  protected $solrModelPluginManager;

  protected $entityTypeManager;

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    SolrModelPluginManager $solrModelPluginManager,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->solrModelPluginManager = $solrModelPluginManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  abstract protected function getRelations();

  protected function getStorage($entityTypeId) : EntityStorageInterface {
    if (!isset(static::$entityStorages[$entityTypeId])) {
      static::$entityStorages[$entityTypeId] = $this->entityTypeManager->getStorage($entityTypeId);
    }
    return static::$entityStorages[$entityTypeId];
  }

  public function getRelated(): array {
    $related = [];
    foreach ($this->getRelations() as $solrModelId => $fields) {
      $definition = $this->solrModelPluginManager->getDefinition($solrModelId);
      $storage = $this->getStorage($definition['entityType']);
      foreach ($fields as $field) {
        $query = $storage->getQuery();
        $query->condition($field, $this->getId());
        $entities = $storage->loadMultiple($query->execute());
        $related += array_map(function(ContentEntityInterface $entity){
          return $this->solrModelPluginManager->wrap($entity);
        }, $entities);
      }
    }
    return [];
  }

  public static function alterIndexQuery(QueryInterface $countQuery): void {

  }

  public function shouldIndex(): bool {
    return TRUE;
  }

  /**
   * @SolrField("name_txt_en_split")
   * @SolrField("name_txt_en_edgy")
   * @SolrField("name_s")
   */
  public function getName() {
    return $this->getEntity()->getName();
  }

}
