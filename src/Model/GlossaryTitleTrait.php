<?php

namespace Drupal\ctek_search\Model;

use Drupal\ctek_search\Annotation\SolrField;

trait GlossaryTitleTrait {

  /**
   * @SolrField("glossary_s")
   */
  public function getTitleForGlossary() {
    return strtoupper(substr($this->getTitle(), 0, 1));
  }

}
