<?php

namespace Drupal\ctek_search\Model;

use Drupal\ctek_common\Model\ModelBase;
use Drupal\ctek_search\SolrModelInterface;

abstract class SolrModelBase extends ModelBase implements SolrModelInterface {
  use AnnotatedIndexableModelTrait;

}
