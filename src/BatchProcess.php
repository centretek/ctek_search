<?php

namespace Drupal\ctek_search;

use Drupal\Core\Cache\Cache;
use Drupal\ctek_common\Batch\ManagedBatch;
use Drupal\ctek_common\Batch\ManagedBatchProcessInterface;
use Drupal\ctek_common\Batch\ManagedBatchProcessTrait;
use Drupal\ctek_common\Batch\PagedBatchJob;
use Drupal\ctek_common\Batch\PagedBatchOperation;
use Drupal\ctek_common\Batch\SimpleBatchJob;
use Drupal\ctek_search\Solr\Indexer;
use Symfony\Component\HttpFoundation\ParameterBag;

class BatchProcess implements ManagedBatchProcessInterface {
  use ManagedBatchProcessTrait;

  const CONFIG_KEY_DEFINITIONS = 'definitions';

  protected static $indexer;

  /**
   * @return \Drupal\ctek_search\Solr\Indexer
   */
  protected static function indexer() {
    if (!static::$indexer) {
      static::$indexer = \Drupal::service('ctek_search.indexer');
    }
    return static::$indexer;
  }

  public static function getName(): string {
    return 'Ctek Search Indexer';
  }

  public static function init(ManagedBatch $batch): array {
    $jobs = [];
    $definitions = $batch->config->get(static::CONFIG_KEY_DEFINITIONS);
    $job = SimpleBatchJob::create([static::class, 'clearIndices']);
    $job->arguments()->set('definitions', $definitions);
    $jobs[] = $job;
    foreach ($definitions as $definition) {
      $ids = static::indexer()
        ->getDbQueryForIndex($definition)
        ->execute();
      $job = PagedBatchJob::create([static::class, 'index'])
        ->setTotal(count($ids))
        ->setPageSize(static::BATCH_SIZE);
      $job->arguments()->set('definition', $definition);
      $jobs[] = $job;
    }
    $jobs[] = SimpleBatchJob::create([static::class, 'commitChanges']);
    foreach ($definitions as $definition) {
      $job = SimpleBatchJob::create([static::class, 'clearCache']);
      $job->arguments()->set('definition', $definition);
      $jobs[] = $job;
    }

    return $jobs;
  }

  public static function clearIndices(ManagedBatch $batch, ParameterBag $args) {
    static::logger()->notice('Clearing indices...');
    foreach ($args->get('definitions') as $definition) {
      static::indexer()->clearIndex($definition, FALSE);
      \Drupal::logger(Constants::LOGGER_CHANNEL)->notice('Cleared index: @entityType.@bundle', [
        '@entityType' => $definition['entityType'],
        '@bundle' => $definition['bundle'],
      ]);
    }
  }

  public static function index(
    ManagedBatch $batch,
    PagedBatchOperation $operation
  ) {
    /** @var PagedBatchJob $job */
    $job = $operation->getJob();
    $definition = $operation
      ->getJob()
      ->arguments()
      ->get('definition');
    static::logger()->notice('Indexing @entityType:@bundle @start to @end of @total.', [
      '@bundle' => $definition['bundle'],
      '@entityType' => $definition['entityType'],
      '@start' => ($operation->getPage() * $operation->getPageSize()) + 1,
      '@end' => min(($operation->getPage() + 1) * $operation->getPageSize(), $operation->getTotal()),
      '@total' => $job->getTotal(),
    ]);
    $ids = static::indexer()
      ->getDbQueryForIndex($definition)
      ->range($operation->getPageSize() * $operation->getPage(), $operation->getPageSize())
      ->execute();
    $entities = \Drupal::entityTypeManager()
      ->getStorage($definition['entityType'])
      ->loadMultiple($ids);
    static::indexer()->indexEntities($entities, FALSE, FALSE, FALSE);
  }

  public static function commitChanges() {
    static::logger()->notice('Committing changes...');
    static::indexer()->commit();
  }

  public static function clearCache(ManagedBatch $batch, ParameterBag $args) {
    $cid = Indexer::getIndexCacheId($args->get('definition'));
    static::logger()->notice('Invalidating cache: @cid', ['@cid' => $cid]);
    Cache::invalidateTags([$cid]);
  }

}
