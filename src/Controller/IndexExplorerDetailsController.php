<?php

namespace Drupal\ctek_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\ctek_search\Solr\Client;
use Drupal\ctek_search\SolrModelPluginManager;
use Solarium\QueryType\Select\Result\Document;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IndexExplorerDetailsController extends ControllerBase {

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ctek_search.client'),
      $container->get('plugin.manager.solr-model')
    );
  }

  protected $client;

  protected $searcher;

  protected $solrModelPluginManager;

  protected $details = [];

  public function __construct(
    Client $client,
    SolrModelPluginManager $solrModelPluginManager
  ) {
    $this->client = $client;
    $this->solrModelPluginManager = $solrModelPluginManager;
  }

  protected function getDetails($id) {
    if (!isset($this->details[$id])) {
      $select = $this->client
        ->createSelect()
        ->setFields(['*'])
        ->setRows(1)
        ->setQuery('id:%T1%', [$id]);
      /** @var \Solarium\QueryType\Select\Result\Document $document */
      foreach ($this->client->select($select)->getDocuments() as $document) {
        $fields = $document->getFields();
        $this->details[$fields['id']] = $document;
      }
    }
    return $this->details[$id];
  }

  public function details($id) {
    return $this->formatResult($this->getDetails($id));
  }

  public function title($id) {
    $fields = $this->getDetails($id)->getFields();
    return $fields['title_s'] ?? $fields['name_s'];
  }

  protected function formatResult(Document $result) {
    $fields = $result->getFields();
    $fieldPairs = [];
    foreach ($fields as $key => $value) {
      if (is_array($value)) {
        $value = join(', ', $value);
      }
      $fieldPairs[] = [
        $key,
        $value,
      ];
    }
    $table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Field'),
        $this->t('Value'),
      ],
      '#rows' => $fieldPairs,
    ];
    return $table;
  }

}
