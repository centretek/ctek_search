<?php

namespace Drupal\ctek_search;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\ctek_common\Model\ModelInterface;
use Solarium\QueryType\Update\Query\Document;

interface SolrModelInterface extends ModelInterface, CacheableDependencyInterface {

  /**
   * @return bool
   */
  public function shouldIndex() : bool;

  /**
   * @return string
   */
  public function idForIndexing() : string;

  /**
   * @return string
   */
  public function site() : string;

  /**
   * @return string
   */
  public function environment() : string;

  /**
   * @return Document
   */
  public function getSolrDocument() : Document;

  /**
   * @return \Drupal\ctek_search\SolrModelInterface[]
   */
  public function getRelated() : array;

  /**
   * @param \Drupal\Core\Entity\Query\QueryInterface $countQuery
   * @return void
   */
  public static function alterIndexQuery(QueryInterface $countQuery) : void;

}
